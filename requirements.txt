-e git+git://github.com/fredj/cssmin.git@master#egg=cssmin
-e git+git://github.com/SmileyChris/django-countries.git@a2c924074dbe2f0b9b3059bf70064aeadf5643ed#egg=django-countries
Django==1.7
IPy==0.81
Markdown==2.4.1
bencode==1.0
jsmin==2.0.11
pgpdump==1.5
pytz>=2014.7
